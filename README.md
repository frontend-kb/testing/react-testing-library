[[_TOC_]]

## General

- get familiar with [types of queries](https://testing-library.com/docs/queries/about#types-of-queries)

## [Common mistakes with React Testing Library](https://kentcdodds.com/blog/common-mistakes-with-react-testing-library)

**tl;dr**

- use eslint plugins
- descruct `render` call into vars that you need
- use `screen` despite `render` results to test things
- don't use `cleanup` by yourself
- use assertion methods like `toBeDisabled` despite boolean comparison to improve results readability
- prefer `getByRole` or etc over `getByTestId`
- prefer `userEvent` over `fireEvent`
- prefer `find*` selectors over `waitFor`
- use `query*` selectors only for non-existence checks

## Custom hooks

Use `renderHook` from `@testing-library/react`

```javascript
const { result } = renderHook(() => {
    return useYourHook();
});

// note, result is a ref
expect(result.current).toStrictEqual('whatever the result should be');
```

## useEffect

**Just don't**

See [How to Test React.useEffect](https://epicreact.dev/how-to-test-react-use-effect/)

**tl;dr**

Don't test implementation details, consider what you exactly expect for particular state or after firing some event

## Use with Jest
See [jest-dom](https://www.npmjs.com/package/@testing-library/jest-dom)

The lib provides a bunch of matcher to make your life easier